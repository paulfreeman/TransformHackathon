## TransformHackathon
Winning entry in the Hacakthon, this repo contains code generated during Transform Hackathon by Team HealthEye 

In the team were Paul Freeman and Irfan Ansari, Paul devised the project concept and designed and built the iOS client and Irfan developed the backend REST api. 

###App concept 

The app used Estimote sticker beacons which were attached to various objects around the Hacakthon workspace that modelled objects in a hospital. We modelled three kinds of things 

1. Patients  
2. Beds
3. Trolleys 

We envisaged the Patients being given a wristband in future which includes an embedded Estimote sticker beacon. 
 
In addition spaces in the hospital were identified with fixed Estimote beacons 

For the purpose of the hack we used an iPhone app written in Swift to perform several use cases which normally would be fulfilled by a combination apps. 

1. The app scans for the patients, beds and trolleys as the user of the app (A nurse perhaps) moves around the hospital. We envisage that while mobile apps might scan for objects as hospital staff move around the hospital that there would also be embedded computers co-located with the fixed beacons that would scan for the moving sticker beacons. 

2. The app allows a Nurse to search for a specific kind of object (eg: a patient or a trolley) to find where it is located at any one point in time. 

3. A senior nurse is notified on their mobile device if something worrying is happening in the hospital, for example a patient has been on a trolley in a holding area such as a corridor for a long time and might need attention. In reality such events hardly ever happen but in the rare case where they do this could be valuable in ensuring the safety of the patient. 

###Operation of the system

As a moving iPhone/device, or a fixed embedded monitoring computer detects objects it reports these detections via a REST API to a cloud server. Because detections of objects are intrinsically 'dirty' data with signal strength, the interposition of various objects and people causing many different events we envisage the system as a whole performing probabilistic and heuristic feature detection based on the initial events where there is a high ratio of noise to signal. 

Note that for many non-critical use cases eg: Finding a spare trolley, wheelchair or other hospital asset, even the 'raw' data provides us with sufficient information to allow the staff member to find the desired object quickly. We only need to refine the data for cases where we need to automate activities or raise alarms. 

Once features are detected eg: Co-location of a patient, a trolley in a holding area such as a corridor we identify a hypothesis that can be confirmed by future features detected later in the time series. 

Using big-data techniques we can identify macro 'events' such as "Patient Fred Blogs has been on Trolley 123 for more than 4 hours in the North Corridor." When there is sufficient probability of this event having occurred we would then use the backend notification service to notify all hospital staff that this is happening.   


The docs directory contains the slides of our final presentation. 

A video of the working components can be seen here 

https://youtu.be/W5B-KaP07S0 

###Building the client code 

The iOS app uses cocoapods to install the Parse and Estimote SDK's.  So to build the app you need to open the nhspatientsafety.xcworkspace file not the xcodeprojects

There were a few name changes in the spirit of a hackathon so there could be issues with updating the podfile, if you hit these just move the Swift source files and various resources into a new XCode project and use the podfile in the root to build a new project. 


