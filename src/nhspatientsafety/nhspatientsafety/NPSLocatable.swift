//
//  NPSLocatable.swift
//  nhspatientsafety
//
//  Created by Paul on 25/04/2015.
//  Copyright (c) 2015 com.rocketgardenlabs. All rights reserved.
//

import Foundation

class NPSLocatable {
    
    var identifier : String
    var RSSI       : Int
    var type       : String
    
    init(identifier: String, RSSI:Int, type:String) {
        self.identifier = identifier
        self.RSSI = RSSI
        self.type = type
    }
    
}