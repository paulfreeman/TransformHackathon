//
//  NPSTableViewCell.swift
//  nhspatientsafety
//
//  Created by Paul on 26/04/2015.
//  Copyright (c) 2015 com.rocketgardenlabs. All rights reserved.
//

import UIKit

// MARK: - TVC class


class NPSTableViewCell: UITableViewCell
{
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        
        super.init(style: UITableViewCellStyle.Subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}