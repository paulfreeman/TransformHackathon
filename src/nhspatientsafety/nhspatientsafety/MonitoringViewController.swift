//
//  MonitoringViewController.swift
//  nhspatientsafety
//
//  Created by Paul on 25/04/2015.
//  Copyright (c) 2015 com.rocketgardenlabs. All rights reserved.
//

import UIKit


class MonitoringViewController: UIViewController,
        UITableViewDelegate,
        UITableViewDataSource,
        ESTNearableManagerDelegate,
        ESTBeaconManagerDelegate {
    
        var launchScreen : UIView?
    
        @IBOutlet weak var topview: UIView!
        @IBOutlet weak var bottomView: UIView!
    
        var nearables:Array<ESTNearable>!
        var nearableManager:ESTNearableManager!
        var beaconManager: ESTBeaconManager!
        var tableView:UITableView!
    
    
        @IBAction func trolleySearch(sender: AnyObject) {
            
            let alerter = UIAlertController(title: "Found Trolley", message: "Trolley available in North Corridor near to refectory", preferredStyle: UIAlertControllerStyle.Alert)
            alerter.addAction(UIAlertAction(title:"Done", style:UIAlertActionStyle.Default, handler:nil))
            presentViewController(alerter, animated:true, completion:nil)
            
        }
    
    
        // MARK: - Table View delegate callbacks 
        
        func numberOfSectionsInTableView(tableView: UITableView) -> Int
        {
            return 1
        }
        
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        {
                return nearables.count
        }
        
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier", forIndexPath: indexPath) as! NPSTableViewCell
            
            let nearable = nearables[indexPath.row] as ESTNearable
            
            cell.textLabel?.text       =  "\(self.labelForNearableType(nearable.type))"
            cell.detailTextLabel?.text =  "id:\(nearable.identifier) rssi:\(nearable.rssi)"
            
            if let imageView = cell.viewWithTag(1001) as? UIImageView {
                imageView.image = self.imageForNearableType(nearable.type)
            }
            else {
                let imageView = UIImageView(frame: CGRectMake(self.view.frame.size.width - 60, 30, 30, 30))
                imageView.contentMode = UIViewContentMode.ScaleAspectFill
                imageView.image = self.imageForNearableType(nearable.type)
                imageView.tag = 1001
                cell.contentView.addSubview(imageView)
            }
            
            return cell
        }
        
        func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
        {
            return 80
        }
        
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
        {
            self.performSegueWithIdentifier("details", sender: indexPath)
        }
        
        // MARK: - Navigation
        
        override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
        {
            if(segue.identifier == "details")
            {
                //var monitVC:MonitoringDetailsViewController = segue.destinationViewController as! MonitoringDetailsViewController
                //monitVC.nearable = self.nearables[(sender as! NSIndexPath).row]
            }
        }
        
        // MARK: - ESTNearableManager delegate
        
        func nearableManager(manager: ESTNearableManager!, didRangeNearables nearables: [AnyObject]!, withType type: ESTNearableType)
        {
            self.nearables = nearables as! Array<ESTNearable>
            
            for nearable in nearables {
                println(nearable)
            }
            
            let els = [ESTNearableType.Bag, ESTNearableType.Fridge, ESTNearableType.Shoe]
            
            self.nearables = self.nearables.filter({
                contains(els, $0.type)
            })
            
            if(self.nearables.count > 0){
                
                //TODO: Add real data
                let l1 = NPSLocatable(identifier: "1", RSSI: -95, type: "trolley")
                let l2 = NPSLocatable(identifier: "2", RSSI: -80, type: "patient")
                let l3 = NPSLocatable(identifier: "3", RSSI: -40, type: "beacon")
                
                let item = NPSEvent(identifier:"1", locatables:[l1, l2, l3])
                
                NPSAPI().updateAPI(item)
            }
            
            self.tableView.reloadData()
        }
    
    
        
        func imageForNearableType(type: ESTNearableType) -> UIImage?
        {
            switch (type)
            {
            case ESTNearableType.Bag:
                return  UIImage(named: "trolley")
            case ESTNearableType.Fridge:
                return UIImage(named: "patient")
            case ESTNearableType.Shoe:
                return UIImage(named: "bed")
            default:
                return UIImage(named: "sticker_grey")
            }
        }
    
    func labelForNearableType(type : ESTNearableType) -> String {
            switch(type)
            {
            case ESTNearableType.Bag:
                return  "trolley"
            case ESTNearableType.Fridge:
                return "patient"
            case ESTNearableType.Shoe:
                return  "bed"
            default:
             return  "unknown"
        }
    }

    
    // MARK: - Loading state management
    
    func insertLaunchScreen() {
        self.launchScreen = UINib(nibName: "LaunchScreen", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as? UIView
        self.launchScreen!.frame = self.view.frame
        self.view.addSubview(launchScreen!)
        var timer = NSTimer.scheduledTimerWithTimeInterval(1.1, target: self, selector: Selector("someSelector"), userInfo: nil, repeats: false)
        
    }
    
    func someSelector() {
        self.launchScreen?.removeFromSuperview()
        self.launchScreen = nil
    }
 
    func startMonitoring () -> Void {
    
        self.title = "Patients and hospital assets nearby";
      
        tableView = UITableView(frame: CGRectMake(0,
            self.topview.frame.origin.y + self.topview.frame.size.height + 30,
            self.topview.frame.size.width,
            300))
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        
        tableView.registerClass(NPSTableViewCell.classForCoder(), forCellReuseIdentifier: "CellIdentifier")
        
        nearables = []
        nearableManager = ESTNearableManager()
        nearableManager.delegate = self
        nearableManager.startRangingForType(ESTNearableType.All)
    
    }


    // MARK: - Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = true;
        super.viewWillAppear(animated)
    }
    
    
    override func viewDidLoad() { 
        
        super.viewDidLoad()
        self.startMonitoring()
        self.insertLaunchScreen()
        // Do any additional setup after loading the view, typically from a nib.
        
       
        
    }
}