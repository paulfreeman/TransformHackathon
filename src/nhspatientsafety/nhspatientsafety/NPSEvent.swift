//
//  NPSEvent.swift
//  nhspatientsafety
//
//  Created by Paul on 25/04/2015.
//  Copyright (c) 2015 com.rocketgardenlabs. All rights reserved.
//

import Foundation

public class NPSEvent {
    var identifier   : String
    var locatables   :[NPSLocatable]
    
    init(identifier: String, locatables:[NPSLocatable]){
        self.identifier = identifier
        self.locatables = locatables
    }
    
}
